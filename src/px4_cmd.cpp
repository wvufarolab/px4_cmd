#include <px4_cmd/px4_cmd.hpp>

DronePX4::DronePX4(ros::NodeHandle &nh) : nh_(nh)
{
    mavros_state_sub = nh_.subscribe<mavros_msgs::State>("mavros/state", 10, &DronePX4::mavrosStateCallback, this);
    mavros_rcin_sub = nh_.subscribe<mavros_msgs::RCIn>("mavros/rc/in", 10, &DronePX4::mavrosRCInCallback, this);
    drone_msg_sub = nh_.subscribe<state_machine::DroneMsg>("command", 10, &DronePX4::droneMsgCallback, this);
    
    setpoint_raw_pub = nh_.advertise<mavros_msgs::PositionTarget>("mavros/setpoint_raw/local", 10);

    arming_client = nh_.serviceClient<mavros_msgs::CommandBool>("mavros/cmd/arming");
    set_mode_client = nh_.serviceClient<mavros_msgs::SetMode>("mavros/set_mode");
    takeoff_client = nh_.serviceClient<mavros_msgs::CommandTOL>("mavros/cmd/takeoff");
    landing_client = nh_.serviceClient<mavros_msgs::CommandTOL>("mavros/cmd/landing");

    cmd_timer_ = ros::Time::now();

    rc_mode_ = RC_MODES::offboard;
}

void DronePX4::mavrosStateCallback(const mavros_msgs::State::ConstPtr &msg)
{
    current_state_ = *msg;
    // ROS_INFO_STREAM(current_state_);
}

void DronePX4::mavrosRCInCallback(const mavros_msgs::RCIn::ConstPtr &msg)
{
    rc_in_state_ = *msg;
    // ROS_INFO_STREAM(rc_in_state_);

    if (msg->channels[5] > 1750)
    {
        rc_mode_ = RC_MODES::auto_hold;
    }
    else if (msg->channels[5] < 1250)
    {
        rc_mode_ = RC_MODES::auto_land;
    }
    else
    {
        rc_mode_ = RC_MODES::offboard;
    }
}

void DronePX4::droneMsgCallback(const state_machine::DroneMsg::ConstPtr &msg)
{
    drone_msg_ = *msg;
    // ROS_INFO_STREAM(drone_msg_);

    if (rc_mode_ == RC_MODES::offboard)
    {
        if (msg->cmd_type.data == DRONE_MODES::autotakeoff)
        {
            ROS_INFO("[PX4 CMD] Commanding ARM.");
            armCmd();

            ROS_INFO("[PX4 CMD] Setting AUTO.TAKEOFF mode.");
            takeoffCmd();
        }
        else if (msg->cmd_type.data == DRONE_MODES::position)
        {
            if(current_state_.mode != "OFFBOARD")
            {
                // Send a couple setpoints before changing to OFFBOARD
                for(int i=0; i<10; i++)
                {
                    positionCmd();
                }
                ROS_INFO("[PX4 CMD] Setting OFFBOARD mode.");
                offboardCmd(); 
            }

            ROS_INFO_THROTTLE(0.5, "[PX4 CMD] Sending Position Setpoint.");
            positionCmd();
        }
        else if (msg->cmd_type.data == DRONE_MODES::velocity)
        {
            if(current_state_.mode != "OFFBOARD")
            {
                // Send a couple setpoints before changing to OFFBOARD
                for(int i=0; i<10; i++)
                {
                    velocityCmd();
                }
                ROS_INFO("[PX4 CMD] Setting OFFBOARD mode.");
                offboardCmd(); 
            }

            ROS_INFO_THROTTLE(0.5, "[PX4 CMD] Sending Velocity Setpoint.");
            velocityCmd();
        }
        else if (msg->cmd_type.data == DRONE_MODES::autoland)
        {
            ROS_INFO("[PX4 CMD] Setting AUTO.LAND mode.");
            landCmd();
            
            ros::Duration(3.0).sleep();
            ROS_INFO("[PX4 CMD] Commanding DISARM.");
            disarmCmd();
        }
        else if (msg->cmd_type.data == DRONE_MODES::autoloiter)
        {
            ROS_INFO("[PX4 CMD] Setting AUTO.LOITER mode.");
            holdCmd();
        }
    }

    cmd_timer_ = ros::Time::now();
}

void DronePX4::armCmd()
{
    // Send ARM command
    mavros_msgs::CommandBool cmd;
    cmd.request.value = true;
    arming_client.call(cmd);
}

void DronePX4::disarmCmd()
{
    // Send DISARM command
    mavros_msgs::CommandBool cmd;
    cmd.request.value = false;
    arming_client.call(cmd);
}

void DronePX4::takeoffCmd()
{
    // Set AUTO.TAKEOFF mode
    mavros_msgs::SetMode set_mode;
    set_mode.request.custom_mode = "AUTO.TAKEOFF";
    set_mode_client.call(set_mode);
}

void DronePX4::landCmd()
{
    // Set AUTO.LAND mode
    mavros_msgs::SetMode set_mode;
    set_mode.request.custom_mode = "AUTO.LAND";
    set_mode_client.call(set_mode);
}

void DronePX4::holdCmd()
{
    // Set AUTO.LOITER mode
    mavros_msgs::SetMode set_mode;
    set_mode.request.custom_mode = "AUTO.LOITER";
    set_mode_client.call(set_mode);
}

void DronePX4::offboardCmd()
{
    // Set OFFBOARD mode
    mavros_msgs::SetMode set_mode;
    set_mode.request.custom_mode = "OFFBOARD";
    set_mode_client.call(set_mode);
}

void DronePX4::positionCmd()
{
    // Position setpoint
    mavros_msgs::PositionTarget setpoint_raw_msg;
    setpoint_raw_msg.header.frame_id = "";
    setpoint_raw_msg.coordinate_frame = 8;
    setpoint_raw_msg.type_mask = 3064; // Ignore all but: Px, Py, Pz, Yaw
    setpoint_raw_msg.position.x = drone_msg_.pose.position.x;
    setpoint_raw_msg.position.y = drone_msg_.pose.position.y;
    setpoint_raw_msg.position.z = drone_msg_.pose.position.z;
    setpoint_raw_msg.yaw = drone_msg_.pose.orientation.z;

    setpoint_raw_pub.publish(setpoint_raw_msg);
}

void DronePX4::velocityCmd()
{
    // Velocity setpoint
    mavros_msgs::PositionTarget setpoint_raw_msg;
    setpoint_raw_msg.header.frame_id = "";
    setpoint_raw_msg.coordinate_frame = 8;
    setpoint_raw_msg.type_mask = 1991; // Ignore all but: Vx, Vy, Vz, Yaw_Rate
    setpoint_raw_msg.velocity.x = drone_msg_.velocity.linear.x;
    setpoint_raw_msg.velocity.y = drone_msg_.velocity.linear.y;
    setpoint_raw_msg.velocity.z = drone_msg_.velocity.linear.z;
    setpoint_raw_msg.yaw_rate = drone_msg_.velocity.angular.z;

    setpoint_raw_pub.publish(setpoint_raw_msg);
}

void DronePX4::repeatCmd()
{
    if (rc_mode_ == RC_MODES::offboard && (ros::Time::now() - cmd_timer_) < ros::Duration(10))
    {
        if (drone_msg_.cmd_type.data == DRONE_MODES::position)
        {
            if(current_state_.mode != "OFFBOARD")
            {
                // Send a couple setpoints before changing to OFFBOARD
                for(int i=0; i<10; i++)
                {
                    positionCmd();
                }
                ROS_INFO("[PX4 CMD] Setting OFFBOARD mode.");
                offboardCmd(); 
            }
            ROS_INFO_THROTTLE(0.5, "[PX4 CMD] Commanding Position.");
            positionCmd();
        }
        else if (drone_msg_.cmd_type.data == DRONE_MODES::velocity)
        {
            if(current_state_.mode != "OFFBOARD")
            {
                // Send a couple setpoints before changing to OFFBOARD
                for(int i=0; i<10; i++)
                {
                    velocityCmd();
                }

                ROS_INFO("[PX4 CMD] Setting OFFBOARD mode.");
                offboardCmd(); 
            }
            ROS_INFO_THROTTLE(0.5, "[PX4 CMD] Commanding Velocity.");
            velocityCmd();
        }
    }
}

int main(int argc, char **argv)
{

    // initialize ROS and the node
    ros::init(argc, argv, "px4_cmd");
    ros::NodeHandle nh;

    ros::Rate rate(50.0);

    DronePX4 drone(nh);

    while (ros::ok())
    {
        ros::spinOnce();
        drone.repeatCmd();
        rate.sleep();
    }

    return 0;
}