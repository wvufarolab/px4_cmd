#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <vector>
#include <unistd.h>
#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/convert.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Transform.h>
#include <trajectory_msgs/MultiDOFJointTrajectoryPoint.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/RCIn.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/Altitude.h>
#include <mavros_msgs/PositionTarget.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <state_machine/DroneMsg.h>
#include <state_machine/state_machine.hpp>

enum RC_MODES{auto_hold, auto_land, offboard};

class DronePX4
{
private:
    ros::NodeHandle nh_;

public:
    DronePX4(ros::NodeHandle & nh);

    ros::Subscriber mavros_state_sub;
    ros::Subscriber mavros_rcin_sub;
    ros::Subscriber drone_msg_sub;

    ros::Publisher setpoint_raw_pub;

    ros::ServiceClient arming_client;
    ros::ServiceClient set_mode_client;
    ros::ServiceClient takeoff_client;
    ros::ServiceClient landing_client;

    void mavrosStateCallback(const mavros_msgs::State::ConstPtr& msg);
    void mavrosRCInCallback(const mavros_msgs::RCIn::ConstPtr& msg);
    void droneMsgCallback(const state_machine::DroneMsg::ConstPtr& msg);

    void armCmd();
    void disarmCmd();

    void takeoffCmd();
    void landCmd();
    void holdCmd();
    void offboardCmd();

    void velocityCmd();
    void positionCmd();

    void repeatCmd();

    mavros_msgs::State current_state_;
    mavros_msgs::RCIn rc_in_state_;
    state_machine::DroneMsg drone_msg_;

    int rc_mode_;
    ros::Time cmd_timer_;
};